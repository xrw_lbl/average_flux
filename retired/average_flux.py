# python code to reduce the magmeas data
# v1. 12/8/2009, 15:19. Add the tk dialog asking for the data directory.

import glob
import math
from numpy import *
from numpy.fft import *
from numpy.lib.scimath import *
import os
import struct # for reading binary data
import time
from collections import defaultdict

# global parameters
sf = 20.e6 # 20 MHz sampling frequency for FDI; constant
m_order = 1 # 2-m pole magnet
Nm = 20 # highest harmonic order to be generated
Rref = 1.0 #[m]
t_base = -1.0 # initial value for the time base for normalized time

T_k = empty (Nm, complex)
BQ_k = empty (Nm, complex)

# constant
MAX_CH_NAME_LENGTH = 10
HEADER_SIZE = 2048

#---------------
def load_file (fn):
    f = open (fn, 'r')
    header = []
    r = []  # total set of numbers (r[i]: numbers in i-th row)
    while True:  # might call read several times for a file
        line = f.readline()
        if not line: break  # end of file
        if line.isspace(): break  # blank line
        if line[0] == '#':
            header.append(line)
            continue # treat next line
        r.append(line.split(','))
    f.close ()
    return header, r

# Get the probe position from the data file.
def get_position (fn, position_idx):
    s = (fn.split('_'))[position_idx]
    ss = (s.split('m'))[0]
    return float (ss)

#---------------
#---------------
#---------------
def main (position_idx):
    global m_order, Nm, Rref
    global T_k, BQ_k
    

    # load the last data directory, if any.
    init_dir = 'mms'

    # initialize the parameters
    m_order = 2 # Quadrupole
    Nm = 10 # highest harmonics order 15
    Rref = 21.9 * 0.001 # [m]
    
    # load coil sensitivities
    #T_k = load_kappa ('/home/XRWang/documents/mms/probe_info/820mmp_Tan_k.csv')
    #BQ_k = load_kappa ('/home/XRWang/documents/mms/probe_info/820mmp_BQ_k.csv')
    #print T_k.shape, BQ_k.shape

    # enter the data folder
    path = os.chdir ('data') 
    
    # reduce the data
    src_files = glob.glob('*.csv')
    #src_files.sort (key = get_position)

    en = on = 0
    average = zeros((4096,4),float)
    for src in src_files:
        header, data_text = load_file (src)
        #print data_text
        d = []
        for line in data_text:
            l = []
            for item in line:
                #print item
                l.append(float(item.strip()))
            d.append(l)
        a = array(d)
        #print header
        #0 for I.mag, 1 for T, 2 for DB and 3 for DQB.
        #print a[:,1]

        s = (src.split('_'))[1]
        if s[0] == 'p':
            en = en + 1
            for i in range(1,4):
                average[:,i] = average[:,i] + a[:,i]
        elif s[0] == 'n':
            print src
            on = on + 1
            for i in range(1,4):
                average[:,i] = average[:,i] - a[:,i]
        else:
            print 'Curren polarity missing'
        average[:,0] = average[:,0] + a[:,0]
        #print sum(a[:,1]), sum(a[:,2]), sum(a[:,3])
        #print a[1,-1], a[2,-1], a[3,-1]
    average = average/(float(len(src_files)))
    #print average[1,-1], average[2,-1], average[3,-1], sum(average[:,3])

    posi = get_position (src_files[0], position_idx)
    src_fl = src.split('_')
    new_fn = src_fl[0] + '_' + src_fl[1][1:(len(src_fl[1]))] + '_' + src_fl[2] + '_' + src_fl[3] + '_average.flux'
    f = open ('../'+new_fn, 'w')
    f.write ('#position (mm),%.2f\n' % posi)
    f.write ('#date (YYMMDD),%s\n' % src_fl[position_idx+1][0:6])
    f.write ('%s' % ''.join(header))
    for i in range(len(average[:,1])):
        f.write ('%.1f,%.16e,%.16e,%.16e\n' % (average[i,0],average[i,1],average[i,2],average[i,3]))
    f.close()
    print '%d + files and %d - files processed. Averaged file: %s' % (en, on, new_fn)
#------------

#------------
#position_idx = 1 # the position of the probe position in the file name separated by '_', indexed from 0. 
main (3)
