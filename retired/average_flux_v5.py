# python code to average the flux files from the positive and negative currents.
# 20Jun2018,15:43:35,PDT. Output the averaged CN number of columns. 
# 07/21/16,12:11:01,PDT. minor. The data length of some flux files is larger (not understood). Skip these files if their shape is wrong. 
# 02/24/14,14:55:43,PST. v4. Now read the files with 5 columns of data. The first column is the dt, which is neglected in the averaged flux data file. So the drmms is not changed.  
# 06Feb2014,13:46:59,PST.



import getopt
import glob
import math
from numpy import *
from numpy.fft import *
from numpy.lib.scimath import *
import os
import sys
import string
import time
#from collections import defaultdict

version = '20Jun2018,15:43:35,PDT'

# global parameters
p_folder = ''
n_folder = ''
des_folder = ''
position = []
error = 1.0 #mm
p_position_idx = 3
n_position_idx = 3

# constant

def load_cfg (fn):
    global p_folder, n_folder, des_folder, position, error, p_position_idx, n_position_idx
    global PPT, CN

    cfg = open (fn, 'r');
    r = []
    while True:  # might call read several times for a file
        line = cfg.readline()
        if not line: break  # end of file
        if line[0] == '#':
            continue # treat next line
        r.append(line.split(','))

    for e in r:
        if (e[0] == 'P'):
            p_folder = string.rstrip(e[1],'\n')
        elif (e[0] == 'N'):
            n_folder = string.rstrip(e[1],'\n')
        elif (e[0] == 'D'):
            des_folder = string.rstrip(e[1],'\n')
        elif (e[0] == 'Probe_position'):
            # COunt the length of the list.
            for i in range(1,len(e)):
                position.append(float (e[i]))
            print position
        elif (e[0] == 'E'):
            error = float (e[1])
        elif (e[0] == 'PPI'):
            p_position_idx = int (e[1])
        elif (e[0] == 'NPI'):
            n_position_idx = int (e[1])
        elif (e[0] == 'NP'):
            PPT = int (e[1])
        elif (e[0] == 'CN'):
            CN = int (e[1])
            print CN
        else:
            print 'Identifier ' + e[0] + ' with a value of ' + e[1] + ' not recognized. Neglected.'
    cfg.close ()

#---------------
def load_file (fn):
    f = open (fn, 'r')
    header = []
    r = []  # total set of numbers (r[i]: numbers in i-th row)
    while True:  # might call read several times for a file
        line = f.readline()
        if not line: break  # end of file
        if line.isspace(): break  # blank line
        if line[0] == '#':
            header.append(line)
            continue # treat next line
        r.append(line.split(','))
    f.close ()
    d = []
    for line in r:
        l = []
        for item in line:
            l.append(float(item.strip()))
        d.append(l)
    a = array(d)

    return header, a

# Get the probe position from the data file.
def get_position (fn, idx):
    fn_l = fn.split ('_')
    if (len (fn_l) >= (idx + 1)):
        found = True
        s = fn_l[idx]
#        print s.split('m')
        ss = float ((s.split('m'))[0])
        
    else:
        #print len (fn_l)
        found = False
        ss = -987654321.0
    #print found, ss
    return found, ss

#---------------
#---------------
#---------------
def main (argv):
    global p_folder, n_folder, des_folder, position, error, p_position_idx, n_position_idx
    global PPT, CN
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        sys.exit(2)
    cfg_fn = args[0]

    code_info = os.path.basename(__file__) + ' ' + version
    # load config file
    load_cfg (cfg_fn)

    not_used_files = []
    all_positions = []
    # Locate the positive files
    p_files = []
    for p in position:
        pfl = []
        path = os.chdir (p_folder) 
        src_files = glob.glob('*.csv') #src_files.sort (key = get_position)
    
        for src in src_files:
            found, measured_posi = get_position (src, p_position_idx)
            if (found == True):
                if (abs (measured_posi - p) < error):
                    pfl.append(src)
            else:
                not_used_files.append (src)
            all_positions.append (int (measured_posi))
        p_files.append(pfl)
    #print p_files
    
    # Locate the negative files
    n_files = []
    for p in position:
        nfl = []
        path = os.chdir (n_folder) 
        src_files = glob.glob('*.csv') #src_files.sort (key = get_position)
    
        for src in src_files:
            found, measured_posi = get_position (src, n_position_idx)
            if ((found == True) and (abs (measured_posi - p) < error)):
                nfl.append(src)
            elif (found == False):
                not_used_files.append (src)
        n_files.append(nfl)
    #print n_files

    # average the p and n files
    for i in range (len(position)):
        pl = len(p_files [i])
        nl = len(n_files [i])
        if (pl >= nl):
            num = nl
        else:
            num = pl
        
        average = zeros((PPT,CN),float)

        print '%d files will be loaded for %.2f mm location' % (num, position [i])
        for j in range (num):
            # load the flux with the positive current
            src = p_folder + '/' + p_files [i][j]
            print 'Loading data from %s' % src
            header, data = load_file (src)
            
            if (data.shape == average.shape):
                for k in range(1,CN):
                    average[:,k] = average[:,k] + data[:,k]
                average[:,0] = average[:,0] + data[:,0]
            else:
                print data.shape, average.shape
            # load the flux with the negative current
            src = n_folder + '/' + n_files [i][j]
            print src
            header, data = load_file (src)
            if (data.shape == average.shape):
                for k in range(1,CN):
                    average[:,k] = average[:,k] - data[:,k]
                average[:,0] = average[:,0] + data[:,0]
            else:
                print data.shape, average.shape

        average = average/(float(2 * num))

        ## print header.split(',')
        ## print header [1:]
        src_fl = p_files[i][0].split('_')
        new_fn = src_fl[0] + '_' + src_fl[1][1:(len(src_fl[1]))] + '_' + src_fl[2] + '_' + src_fl[3] + '_' + str(position[i]) + 'mm_average.flux'
        f = open (des_folder + '/' + new_fn, 'w')
        f.write ('#position (mm),%.2f\n' % position [i])
        for m in range(num):
            f.write ('#p%d,%s\n' % (m, p_folder + '/' + p_files [i][m]))
        for m in range(num):
            f.write ('#n%d,%s\n' % (m, n_folder + '/' + n_files [i][m]))
        f.write ('%s' % ''.join(header))
        f.write ('# Generated by %s\n' % code_info)
        for m in range(len(average[:,1])):
            #f.write ('%.1f,%.16e,%.16e,%.16e\n' % (average[m,0],average[m,1],average[m,2],average[m,3]))
            # v4 - below: neglect the first column on dt.
            f.write ('%.1f' % (average[m,1]))
            for k in range(2,CN):
                f.write (',%.16e' % (average[m,k]))
            f.write ('\n')

        f.close()
        print '%d pairs of +/- files processed for %.2f mm data and averaged into: %s' % (num, position[i], new_fn)
        print '====\n====\n'
    print '%d positions processed.' % (i+1)
    print '%d not used files.' % (len (not_used_files))
    print not_used_files
    print position

    print sorted (list (set(all_positions)))
    print 'Done.\n'
#------------

#------------

if __name__ == "__main__":
    main(sys.argv[1:])

#main()
